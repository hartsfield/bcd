// bcd is an app used as a wrapper around youtube-dl to scrape track information
// and album art from bandcamp.
package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

// TODO: write metadata?
//       fix bugs with songs containing invalid characters not being renamed
//       properly (might be fixed, need to do more tests)

// Album is in the structure of an audio album, but also includes the markup
// file from the page that it was scraped.
type Album struct {
	Artist string
	Title  string
	Songs  []string
	Art    string
	Year   string
	Nodes  *html.Node
	Markup string
}

// The main function is mostly just a switch used to check the users aruments.
// No argument   Print help
// help          Print help
// find          Find links to albums on a web page containing bandcamp links
// file          Download albums from all the links in the file
// default       Download the album
func main() {
	if len(os.Args) == 1 {
		printHelp()
	}
	switch os.Args[1] {
	case "help":
		{
			printHelp()
		}
	case "find":
		{
			nodes, _ := getMarkup([]string{os.Args[2]})
			for _, node := range nodes {
				u, _ := url.Parse(os.Args[2])
				for _, v := range findLinks(node, u.Hostname()) {
					fmt.Println(v)
				}
			}
		}
	case "file":
		{
			f, _ := ioutil.ReadFile(os.Args[2])
			linksUnclean := strings.Split(string(f), "\n")
			links := linksUnclean[:len(linksUnclean)-1]

			nodes, markup := getMarkup(links)
			for k, node := range nodes {
				album := makeAlbumData(node, markup[k])
				album.Download(links[k])
			}
		}
	default:
		{
			nodes, markup := getMarkup([]string{os.Args[1]})
			for k, node := range nodes {
				album := makeAlbumData(node, markup[k])
				album.Download(os.Args[1])
			}
		}
	}
}

func printHelp() {
	fmt.Println("BCD - Bandcamp Download wrapper around youtube-dl")
	fmt.Println()
	fmt.Println(`BCD creates a directory in the format 'Artist - Album (Year)', and attempts to
rename and number songs so that they match the track listing displayed on
bandcamp. Requires youtube-dl to be installed and in your $PATH.`)
	fmt.Println()
	fmt.Println("Usage:")
	fmt.Println("    bcd help  -- show this help")
	fmt.Println("    bcd http://bandcamplink.com/ -- download the album from the link")
	fmt.Println("    bcd find http://bandcamplink.com/ -- find links to albums on the page")
	fmt.Println("    bcd file path/to/file -- download all the links in the file (1 link per line)")
	fmt.Println()
	fmt.Println(`!!!WARNING!!! BCD IS STILL EXPERIMENTAL, SOME LINKS MAY BE MISSED
OR IMPROPERLY FORMATED. SOME SONGS WILL NOT BE RENAMED PROPERLY
OR AT ALL. IT IS UP TO YOU TO VERIFY DATA INTEGRITY.`)
	os.Exit(0)
}

// getMarkup is used to ibtain the html text and actual node objects that will
// be parsed.
func getMarkup(links []string) ([]*html.Node, []string) {
	var nodes []*html.Node
	var text []string
	for _, link := range links {
		l := strings.Replace(link, "https", "http", 1)
		r, err := http.Get(l)
		if err != nil {
			log.Panicln(err, "Looks like the page wouldn't load, did you use the correct address? If you are trying to get bcd to read links from a file, run 'bcd file path/to/file.txt'")
		}

		defer func() {
			err := r.Body.Close()
			if err != nil {
				fmt.Println("Error closing response body, this should not effect anything: ", err)
			}
		}()

		buf, _ := ioutil.ReadAll(r.Body)
		// use nopCloser to get an io.readCloser which implements the io.Reader
		// interface used for html.Parse().
		rw1 := ioutil.NopCloser(bytes.NewBuffer(buf))
		n, err := html.Parse(rw1)
		if err != nil {
			fmt.Println(err)
		}
		nodes = append(nodes, n)
		text = append(text, string(buf))
	}
	return nodes, text
}

// Self explanatory? See parse functions for details.
func makeAlbumData(nodes *html.Node, markup string) *Album {
	return &Album{
		Artist: parseArtist(nodes),
		Title:  parseTitle(nodes),
		Year:   parseYear(nodes),
		Songs:  parseSongs(nodes),
		Art:    parseArtLink(nodes),
		Nodes:  nodes,
		Markup: markup,
	}
}

// DirName returns the directory name we'll save our songs to in the format
// "Artist - Album Title (Year)"
func (a *Album) DirName() string {
	return strings.Replace(a.Artist+" - "+a.Title+" ("+a.Year+")", "/", "\\", -1)
}

// Download downloads the Album using the values provided by the receiver.
func (a *Album) Download(link string) {
	// Check if the directory already exists so we don't download the album
	// multiple times.
	err := os.Chdir(a.DirName())
	if err == nil {
		log.Panicf("Directory %s already exists.", a.DirName())
	}

	// Make the directory in the format "Artist - Album Title (Year)"
	err = os.Mkdir(a.DirName(), 0744)
	if err != nil {
		log.Panicln("Couldn't create the directory, are you sure you have permission? ", err)
	}

	// Move into the directory so the files download into it and we can operate
	// on them.
	err = os.Chdir(a.DirName())
	if err != nil {
		log.Panicln("Couldn't move into the new directory, please report this issue: ", err)
	}

	// Use youtube-dl to download the songs.
	// TODO: add native support for song downloading.
	fmt.Printf("Using youtube-dl to download %s...\n", a.DirName())
	c := exec.Command("youtube-dl", link)
	err = c.Run()
	if err != nil {
		log.Panicln("Something went wrong starting youtube-dl. Are you sure it's installed and that it's in your $PATH?: ", err)
	}

	// Rename the songs and get the art.
	a.RenameSongs()
	a.SaveMarkup()
	a.GetArt()
}

// SaveMarkup saves the markup from the website in a file for posterity.
func (a *Album) SaveMarkup() {
	// Save the data to the file
	err := ioutil.WriteFile("markup.html", []byte(a.Markup), 0644)
	if err != nil {
		log.Println("Error saving markup data: ", err)
	}
}

// GetArt downloads the album artwork
func (a *Album) GetArt() {
	// Download the art
	r, err := http.Get(a.Art)
	if err != nil {
		fmt.Println("Couldn't download image from link: ", err)
		return
	}

	defer func() {
		err := r.Body.Close()
		if err != nil {
			fmt.Println("Error closing response body, this should not effect anything: ", err)
		}
	}()

	// Create the file, we call it albumArt and append the extension from the
	// downloaded file to it.
	file, err := os.Create("albumArt" + a.Art[len(a.Art)-4:])
	if err != nil {
		fmt.Println("Error creating album art file: ", err)
		return
	}

	// Save the data to the file
	_, err = io.Copy(file, r.Body)
	if err != nil {
		fmt.Println("Error saving album art: ", err)
	}

	err = file.Close()
	if err != nil {
		log.Println("Couldn't close album art file: ", err)
	}
	os.Chdir("..")
}

// RenameSongs renames and numbers the songs
func (a *Album) RenameSongs() {
	// Read the files from ".", the current working directory. The function that
	// called this one moved us into the directory containing the songs.
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Panicln(err)
	}

	// Remove the trailing number and dash at the end of the file names (this is
	// how they come from youtube-dl)
	for _, v := range files {
		n := strings.Split(v.Name(), "-")
		j := strings.Join(n[:len(n)-1], "-")
		// Need to replace backslashes with underscores to be compatible with
		// youtube-dl
		// TODO: find out what other symbols youtube-dl replaces
		err := os.Rename(v.Name(), strings.Replace(filepath.Clean(j+".mp3"), "/", "_", -1))
		if err != nil {
			fmt.Println("Couldn't rename: "+v.Name(), err)
		}
	}

	// Re-read the dir because we just renamed the files.
	// TODO: Test to see if I really need to do this.
	files, err = ioutil.ReadDir(".")
	if err != nil {
		log.Panicln(err)
	}

	// for each song name, we test whether a filename matches it.
	for l, x := range a.Songs {
		for _, v := range files {
			// remove '.mp3' and compare
			if strings.Compare(v.Name()[:len(v.Name())-4], x) == 0 {
				// if the number is less than 10, add a 0 in front (e.g. 1 -> 01, etc)
				numString := strconv.Itoa(l + 1)
				if len(numString) == 1 {
					err := os.Rename(v.Name(), "0"+numString+" - "+x+".mp3")
					if err != nil {
						fmt.Println("Couldn't rename: "+v.Name(), err)
					}
				} else {
					// else just rename it normally.
					err := os.Rename(v.Name(), numString+" - "+x+".mp3")
					if err != nil {
						fmt.Println("Couldn't rename: "+v.Name(), err)
					}
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Parse functions ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// All the parse functions work basically the same: We hand the response body
// (raw HTML) to a parse function, which is run recursively over the elements
// in the HTML until it finds our data. They each work different enough to
// warrant their own function, but similarly enough that they don't all need
// comments.
func parseTitle(n *html.Node) string {
	var title string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		// Iterate over <h2>'s.
		if n.Type == html.ElementNode && n.Data == "h2" {
			for _, a := range n.Attr {
				// When we find an <h2> containing a key with the value "trackTitle",
				// we accept it as the title and break from our loop.
				if a.Val == "trackTitle" {
					title = n.FirstChild.Data
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return strings.TrimSpace(title)
}

func parseYear(n *html.Node) string {
	var year string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		// Iterate over <div>'s.
		if n.Type == html.ElementNode && n.Data == "div" {
			for _, a := range n.Attr {
				// When we find a <div> containing a key with our values below, we
				// accept it as the year and break from our loop. This is the full date
				// though, we cut out the year at the bottom of this function.
				if a.Val == "tralbumData tralbum-credits" {
					year = n.FirstChild.Data
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	fullDate := strings.TrimSpace(year)
	return fullDate[len(fullDate)-4:]
}

func parseArtist(n *html.Node) string {
	var artist string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		// There must be a better way right? The artist name is in an <a> that has
		// a parent that's a <span> with a key containing the value "byArtist". The
		// <a> has no non-generic elements that can be easily predicted (unless you
		// want to do even more scraping).
		if n.Type == html.ElementNode && n.Data == "a" {
			for _ = range n.Attr {
				if n.Parent.Type == html.ElementNode && n.Parent.Data == "span" {
					for _, b := range n.Parent.Attr {
						if b.Val == "byArtist" {
							artist = n.FirstChild.Data
							break
						}
					}
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return strings.TrimSpace(artist)
}

func parseSongs(n *html.Node) []string {
	// a slice of songs
	var songs []string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "span" {
			for _, a := range n.Attr {
				if a.Val == "name" {
					// Append the song names to the slice.
					songs = append(songs, strings.Replace(filepath.Clean(n.FirstChild.Data), "/", "_", -1))
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return songs
}

func parseArtLink(n *html.Node) string {
	var link string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "img" {
			for l, a := range n.Attr {
				if a.Val == "image" && a.Key == "itemprop" {
					link = n.Attr[l-1].Val
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return link
}

func findLinks(n *html.Node, pref string) []string {
	var links []string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" && (strings.Contains(a.Val, "/album/") || strings.Contains(a.Val, "/track/")) {
					if strings.Contains(a.Val, "http") {
						links = append(links, a.Val)
					} else {
						links = append(links, "http://"+pref+a.Val)
					}
				}
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return links
}
